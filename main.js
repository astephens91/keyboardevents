const input = document.querySelector('input');
const log = document.getElementById('log');
let boxTop = 200;
let boxLeft = 200;

input.addEventListener('keydown', logKey);

function logKey(e) {
  if(e.key == "ArrowDown"){
      boxTop += 10;
  }
  else if (e.key == "ArrowUp"){
      boxTop -= 10;
  }
  else if (e.key == "ArrowRight"){
      boxLeft += 10;
  }
  else if (e.key == "ArrowLeft"){
      boxLeft -= 10;
  }
  document.getElementById("box").style.top = boxTop + "px"
  document.getElementById("box").style.left = boxLeft + "px"
}
